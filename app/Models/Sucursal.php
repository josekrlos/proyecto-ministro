<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sucursal extends Model
{
    use HasFactory;

    protected $fillable = [
        'nombre_lista',
        'nombre_comercial',
        'ubicacion',
        'horario',
        'telefono',
        'website',
        'footer_uno',
        'footer_dos',
        'estado'
    ];

    public $timestamps = false;
    protected $table = "sucursal";
    protected $primaryKey = "id";
}
