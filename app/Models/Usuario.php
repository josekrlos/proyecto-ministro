<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Usuario extends Authenticatable
{
    use HasFactory;

    protected $fillable = [
        'nombres',
        'apellidos',
        'celular',
        'email',
        'rol',
        'nick',
        'password',
        'codigo',
        'estado',
        'sucursal_id'
    ];

    protected $hidden = [
        'password',
    ];

    public $timestamps = false;
    protected $table = "usuario";
    protected $primaryKey = "id";
}
