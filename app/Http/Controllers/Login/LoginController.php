<?php

namespace App\Http\Controllers\Login;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index(){
        if(Auth::check()){
            return redirect('dashboard');
        }else{
            return view('Login.index');
        }
    }

    public function inicio(){
        if(Auth::check()){
            return redirect('dashboard');
        }else{
            return view('/');
        }
    }

}