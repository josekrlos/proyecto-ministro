<!DOCTYPE html>
<html lang="en">
<head>
    @include('Templates.login')
</head>
<body>
    
<form action="" method="POST" autocomplete="off"> 
    @csrf
    @method('POST')
    <div>
        <label for="nick">Usuario</label><input type="text" id="nick" name="nick">
    </div>
    <div>
        <label for="pass">Contraseña</label><input type="password" id="pass" name="pass">
    </div>
    <div>
        <button type="submit">Ingresar</button>
    </div>
</form>

</body>
</html>