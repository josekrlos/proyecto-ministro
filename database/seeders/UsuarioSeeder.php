<?php

namespace Database\Seeders;

use App\Models\Usuario;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsuarioSeeder extends Seeder
{
    public function run(): void
    {
        $total = Usuario::count();
        if($total == 0){
            $codigo = Hash::make('1234');
            $password = Hash::make('123456');
            Usuario::create([
                'nombres' => 'ADMINISTRADOR', 
                'apellidos' => 'SISTEMAS',
                'celular' => '999887777',
                'email' => 'admin@gmail.com',
                'rol' => 'GERENTE',
                'nick' => 'admin',
                'password' => $password,
                'codigo' => $codigo,
                'estado' => '1'
            ]);
        }
    }
}
