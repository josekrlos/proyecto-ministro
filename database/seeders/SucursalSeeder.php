<?php

namespace Database\Seeders;

use App\Models\Sucursal;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SucursalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $total = Sucursal::count();
        if ($total == 0) {
            Sucursal::create([
                "nombre_lista" => "PRINCIPAL",
                "nombre_comercial" => "Principal", 
                "ubicacion" => "Chiclayo", 
                "horario" => "Todo el dia",
                "telefono" => "696969696", 
                "website" => "www.sinnombre.com", 
                "footer_uno" => "",
                "footer_dos" => "", 
                "estado" => "1"
            ]);
        }
    }
}
