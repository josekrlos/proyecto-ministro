<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('usuario', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombres', 45);
            $table->string('apellidos', 45);
            $table->string('celular', 9);
            $table->string('email', 45);
            $table->string('rol', 15);
            $table->string('nick', 12)->nullable();
            $table->string('password', 60)->nullable();
            $table->string('codigo', 60)->nullable();
            $table->string('estado', 2);
            $table->unsignedInteger('sucursal_id')->nullable();
            $table->foreign('sucursal_id')->references('id')->on('sucursal')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('usuario');
    }
};
