<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('sucursal', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre_lista', 20);
            $table->string('nombre_comercial', 45);
            $table->string('ubicacion', 45);
            $table->string('horario', 45);
            $table->string('telefono', 45)->nullable();
            $table->string('website', 45)->nullable();
            $table->string('footer_uno', 45)->nullable();
            $table->string('footer_dos', 45)->nullable();
            $table->string('estado', 2);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('sucursal');
    }
};
